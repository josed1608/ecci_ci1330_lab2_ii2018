﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ECCI_ProyectoPruebaGit.Startup))]
namespace ECCI_ProyectoPruebaGit
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
